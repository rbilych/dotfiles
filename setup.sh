#!/usr/bin/env bash

path=`pwd`

ln -sfv $path/tmux.conf ~/.tmux.conf

rm -v ~/.zshrc
ln -sfv $path/zshrc ~/.zshrc

mkdir -v ~/.config/nvim
ln -sfv $path/init.vim ~/.config/nvim/init.vim

mkdir -v ~/.config/kitty
ln -sfv $path/kitty.conf ~/.config/kitty/kitty.conf

ln -sfv $path/irbrc ~/.irbrc

ln -sfv $path/gemrc ~/.gemrc

ln -sfv $path/redshift.conf ~/.config/redshift.conf

