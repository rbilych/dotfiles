# Dotfiles

### Install packages
`curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -`

`curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -`

`echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`

`sudo apt update`

`sudo apt install git zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn libgdbm-dev libncurses5-dev automake libtool bison libffi-dev neovim zsh tmux kitty`


### Install Ruby ([rvm](https://rvm.io))
`command curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -`

`curl -sSL https://get.rvm.io | bash -s stable`

`source ~/.rvm/scripts/rvm`

`rvm install 3.0.2`

`rvm use 3.0.2 --default`

`rvm @global do gem install rubocop rubocop-performance rubocop-rails rubocop-rspec solargraph`


### Install Postgresql
`sudo apt install postgresql libpq-dev`

`sudo -u postgres createuser ruslan -s`


### Install FiraCode Nerd Font
[NerdFonts](https://www.nerdfonts.com/font-downloads)


### Install [oh-my-zsh](https://ohmyz.sh/)
`sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
#### Install [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)
`git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting`


### Install [base16-shell](https://github.com/chriskempson/base16-shell)
`git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell`


### Install [powerlevel10k](https://github.com/romkatv/powerlevel10k)
`git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k`


### Install [vim-plug](https://github.com/junegunn/vim-plug)
`sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'`


### Clone repository
`git clone https://gitlab.com/rbilych/dotfiles.git ~/Dotfiles`

`cd Dotfiles`

`./setup.sh`


### Install nvim plugins
In nvim run :PlugInstall


### Activate base16 theme
`base16_tomorrow-night`
