# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
export EDITOR='nvim'
ZSH_THEME="powerlevel10k/powerlevel10k"

# Aliases
alias vimrc='nvim ~/.config/nvim/init.vim'
alias v='nvim'
alias vi='nvim'
alias c=' clear'
alias tm='tmux'
alias tmuxrc='nvim ~/.tmux.conf'
alias :q=' exit'

# Shortcats
alias shortcuts='nvim ~/.oh-my-zsh/plugins/common-aliases/common-aliases.plugin.zsh'
alias gitshortcuts='nvim ~/.oh-my-zsh/plugins/git/git.plugin.zsh'
alias rubyshortcuts='nvim ~/.oh-my-zsh/plugins/ruby/ruby.plugin.zsh'
alias railsshortcuts='nvim ~/.oh-my-zsh/plugins/rails/rails.plugin.zsh'

# Plugins
#zsh-syntax-highlighting - https://github.com/zsh-users/zsh-syntax-highlighting
plugins=(git ruby rails common-aliases extract colored-man-pages zsh-syntax-highlighting)

# User configuration
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# RVM
source $HOME/.rvm/scripts/rvm
export PATH="$PATH:$HOME/.rvm/bin"

# Yarn
export PATH="$PATH:$HOME/.yarn/bin"

# terminal support 256 color schemes
export TERM="xterm-256color"

# BASE16 https://github.com/chriskempson/base16-shell
BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)" ]

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
